package com.example.belnek;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import okio.Options;


public class GameSound {
        static SoundPool sp;
        static int cableClick;
        static int complete;
        static int buttonClick;
        public static void Init( Context context){
            sp = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
            cableClick = sp.load(context, R.raw.cableclick, 1);
            buttonClick = sp.load(context, R.raw.buttonclick, 1);
            complete = sp.load(context, R.raw.mon_ons, 1);
            }

        static void PlaySound( int id, float volume){

                sp.play( id, volume, volume, 0, 0, 1);

        }


    }

