package com.example.belnek;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.Map;
import java.util.Timer;

public class GameView extends View {
    static public boolean result;
    public FirebaseFirestore progressDB;
    public int progress;
    private FirebaseAuth mAuth;
    private Map progressMap;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public int n = 6;
    public int w = 100;
    public String email;
    private DocumentReference doc;
    private float canvasSize;
    Rect cr;
    Rect crPCSys;
    Rect mon;

    Bitmap cable0;
    Bitmap cable1;
    Bitmap cable2;
    Bitmap cable3;
    Bitmap cable4;
    Bitmap cable5;
    Bitmap comp_sys;
    Bitmap mon_on;
    Bitmap mon_off;


    int[][] board1 = {{-10, -9, -9, -9, -9,-9},{0, -6, -3, -2, -3, -2}, {2, 2, -5, -6, -7, -2}, {3, 5, -6, -5, -4, -2}, {0, -3, -2, -5, -7, -2},{-11, -9, -9, -9, -9, -9}};
    int[][] board2 = {{-10, -9, -9, -9, -9,-9},{1, -7, -2, -7, -9, -2}, {5, 0, 0, 0, 1, 5}, {-5, -5, -6, -5, -2, 1}, {3, 0, 0, 1, 1, 5},{-11, -9, -9, -9, -9, -9}};
    int[][] board;
    int[][] corr_board;
    int[][] corr_board1 = {{-1, -1, -1, -1, -1,-1}, {0, -1, -1, -1, -1, -1}, {2, 4, -1, -1, -1, -1}, {5, 3, -1, -1, -1, -1}, {0, -1, -1, -1, -1, -1}, {-1, -1, -1, -1, -1,-1}};
    int[][] corr_board2 = {{-1, -1, -1, -1, -1,-1}, {0, -1, -1, -1, -1, -1}, {2, 1, 1, 1, 1, 4}, {-1, -1, -1, -1, -1, 0}, {5, 1, 1, 1, 1, 3}, {-1, -1, -1, -1, -1,-1}};

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        canvasSize = (int) convertDpToPixel(400, context);
        w = (int) (canvasSize / n)-13;
        mAuth = FirebaseAuth.getInstance();

        cable0 = BitmapFactory.decodeResource(getResources(), R.drawable.cable_horz);
        cable1 = BitmapFactory.decodeResource(getResources(), R.drawable.cable_vert);
        cable2 = BitmapFactory.decodeResource(getResources(), R.drawable.cable2);
        cable3 = BitmapFactory.decodeResource(getResources(), R.drawable.cable3);
        cable4 = BitmapFactory.decodeResource(getResources(), R.drawable.cable4);
        cable5 = BitmapFactory.decodeResource(getResources(), R.drawable.cable1);
        comp_sys = BitmapFactory.decodeResource(getResources(), R.drawable.comp_sys);
        mon_on = BitmapFactory.decodeResource(getResources(), R.drawable.mon_on);
        mon_off = BitmapFactory.decodeResource(getResources(), R.drawable.mon_off);
        cr = new Rect(0,0,cable1.getWidth(), cable1.getHeight());
        crPCSys = new Rect(0,0,comp_sys.getWidth(), comp_sys.getHeight());
        mon = new Rect(0,0,mon_on.getWidth(), mon_on.getHeight());
        Resources resources = context.getResources();
    }

    public float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        switch (Levels.level){
            case 1:
                board = board1;
                corr_board = corr_board1;
                break;
            case 2:
                board = board2;
                corr_board = corr_board2;

        }
        canvas.drawARGB(0, 127, 199, 255);
        Paint p = new Paint();
        p.setColor(Color.WHITE);
    
        Paint p1 = new Paint();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                //canvas.drawRect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p);
                if (board[i][j] == 0 || board[i][j] == -2) {
                    p1.setColor(Color.RED);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable0, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == 1 || board[i][j] == -3) {
                    p1.setColor(Color.GREEN);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable1, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);
                }
                if (board[i][j] == 2 || board[i][j] == -4) {
                    p1.setColor(Color.BLACK);
                   // canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable2, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == 3 || board[i][j] == -5) {
                    p1.setColor(Color.BLUE);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable3, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == 4 || board[i][j] == -6) {
                    p1.setColor(Color.YELLOW);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable4, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == 5 || board[i][j] == -7) {
                    p1.setColor(Color.WHITE);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(cable5, cr, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == -10) {
                    p1.setColor(Color.WHITE);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(comp_sys, crPCSys, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == -11) {
                    p1.setColor(Color.WHITE);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(mon_off, mon, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
                if (board[i][j] == -12) {
                    p1.setColor(Color.WHITE);
                    //canvas.drawOval(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 1, p1);
                    canvas.drawBitmap(mon_on, mon, new Rect(i * w + 1, j * w + 1, (i + 1) * w - 1, (j + 1) * w - 11), p1);

                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!result){
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int i = (int) (event.getX() / w);
                int j = (int) (event.getY() / w);
                switch (board[i][j]) {
                    case 0:
                        board[i][j] = 1;
                        GameSound.PlaySound(GameSound.cableClick, 1);
                        break;
                    case 1:
                        board[i][j] = 0;
                        GameSound.PlaySound(GameSound.cableClick, 1);
                        break;
                    case 2:
                        board[i][j] = 3;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case 3:
                        board[i][j] = 4;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case 4:
                        board[i][j] = 5;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case 5:
                        board[i][j] = 2;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -2:
                        board[i][j] = -3;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -3:
                        board[i][j] = -2;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -4:
                        board[i][j] = -5;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -5:
                        board[i][j] = -6;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -6:
                        board[i][j] = -7;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;
                    case -7:
                        board[i][j] = -4;
                        GameSound.PlaySound(GameSound.cableClick, 1);

                        break;

                }


                result = ArraysS(board, corr_board);
                if (result) {
                    for (int c = 0; c < n; c++) {
                        for (int d = 0; d < n; d++) {
                            if (board[c][d] == -11)
                                board[c][d] = -12;
                        }
                    }
                }
            }

        System.out.println(Arrays.deepEquals(board, corr_board));
        System.out.println(Arrays.deepToString(board));
        invalidate();
    }
        return true;

    }
    public boolean ArraysS(int board[][], int corr_board[][]){
        int solve;
        int corr_solve;
        pr:
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                solve = board[i][j];
                corr_solve = corr_board[i][j];
                if (solve < 0)
                    solve = -1;
                if(solve != corr_solve)
                    return false;
            }
        }

        return true;

    }
}
