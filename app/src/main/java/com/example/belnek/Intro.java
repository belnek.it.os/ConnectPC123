package com.example.belnek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class Intro extends AppCompatActivity {
    private ImageView logo;
    public static boolean end = false;
    private Timer mTimer;
    private Intro.MyTimerTask1 mMyTimerTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        logo = (ImageView) findViewById(R.id.logo) ;
        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask1();
        mTimer.scheduleAtFixedRate(mMyTimerTask, 0, 15);


    }
    class MyTimerTask1 extends TimerTask {

        @Override
        public void run() {
            if(logo.getAlpha() < 1 && !end){

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        logo.setAlpha((float) (logo.getAlpha() + 0.01));
                    }
                });
            }

            else{
                end = true;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        logo.setAlpha((float) (logo.getAlpha() - 0.01));
                        if(logo.getAlpha() < 0.01){
                            startActivity(new Intent(Intro.this, StartActivity.class));
                            mTimer.cancel();
                        }}
                });
            }

        }
    }
}