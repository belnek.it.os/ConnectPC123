package com.example.belnek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Level1 extends AppCompatActivity {
    private  static boolean result;
    public static GameView gameView;
    public FirebaseFirestore progressDB;
    private TextView levelNumber;
    private DocumentReference doc;
    private Button buttonBack;
    private Button buttonStart;
    private Button buttonReturn;
    private Button buttonNextLevel;
    public static long progress = 1;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;
    public Map<String, Object> progressMap = new HashMap<>();
    public Dialog dialog;
    public Dialog dialog_win;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);

        //setContentView(new GameView(this));
        gameView = (GameView) findViewById(R.id.gameView);
        levelNumber = (TextView) findViewById(R.id.NumberLevel);
        //gameView =new GameView(this);
        progressDB  = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();
        mTimer.scheduleAtFixedRate(mMyTimerTask, 0, 1);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_instruct);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog_win = new Dialog(this);
        dialog_win.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_win.setContentView(R.layout.dialog_win);
        dialog_win.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_win.setCancelable(false);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonStart = (Button) dialog.findViewById(R.id.buttonStart);
        buttonNextLevel = (Button) dialog_win.findViewById(R.id.buttonNextLevel);
        buttonReturn = (Button) dialog_win.findViewById(R.id.buttonReturn);
        Integer level = Levels.level;
        levelNumber.setText(level.toString());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             GameSound.PlaySound(GameSound.buttonClick, 1);

                switch (v.getId()){

                    case R.id.buttonStart:
                        dialog.dismiss();
                        break;
                    case R.id.buttonNextLevel:
                        doc = progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail());
                        doc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();

                                    progressMap = document.getData();
                                    progress = (long) progressMap.get("progress");
                                }
                            }
                        });


                        switch (Levels.level) {
                            case 1:
                                if(progress == 1)
                                    progress = 2;
                                break;
                            case 2:
                                if(progress == 2)
                                    progress = 3;
                                break;
                            case 3:
                                if(progress == 3)
                                    progress = 4;
                                break;
                            case 4:
                                if(progress == 4)
                                    progress = 5;
                                break;
                            case 5:
                                if(progress == 5)
                                    progress = 6;
                                break;
                            case 6:
                                if(progress == 6)
                                    progress = 7;
                                break;
                            case 7:
                                if(progress == 7)
                                    progress = 8;
                                break;
                            case 8:
                                if(progress == 8)
                                    progress = 9;
                                break;
                            case 9:
                                if(progress == 9)
                                    progress = 10;
                                break;
                            case 10:
                                break;
                        }

                        progressMap.put("progress", progress);
                        progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail()).set(progressMap);
                        Levels.level = Levels.level + 1;
                        recreate();
                        break;
                    case R.id.buttonReturn:
                        doc = progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail());
                        doc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot document = task.getResult();

                                    progressMap = document.getData();
                                    progress = (long) progressMap.get("progress");
                                    System.out.println(progress);

                                }
                            }
                        });
                        switch (Levels.level) {
                            case 1:
                                if(progress == 1)
                                    progress = 2;
                                break;
                            case 2:
                                if(progress == 2)
                                    progress = 3;
                                break;
                            case 3:
                                if(progress == 3)
                                    progress = 4;
                                break;
                            case 4:
                                if(progress == 4)
                                    progress = 5;
                                break;
                            case 5:
                                if(progress == 5)
                                    progress = 6;
                                break;
                            case 6:
                                if(progress == 6)
                                    progress = 7;
                                break;
                            case 7:
                                if(progress == 7)
                                    progress = 8;
                                break;
                            case 8:
                                if(progress == 8)
                                    progress = 9;
                                break;
                            case 9:
                                if(progress == 9)
                                    progress = 10;
                                break;
                            case 10:
                                break;
                        }

                        progressMap.put("progress", progress);
                        progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail()).set(progressMap);
                        Intent intent = new Intent(Level1.this, Levels.class);
                        startActivity(intent);
                        break;
                    case R.id.buttonBack:
                            startActivity(new Intent(Level1.this, Levels.class));
                            break;
                }



                }

        };

        buttonStart.setOnClickListener(listener);
        buttonNextLevel.setOnClickListener(listener);
        buttonReturn.setOnClickListener(listener);
        buttonBack.setOnClickListener(listener);
        if(Levels.level == 1)
            dialog.show();

    }
    @Override
    public void onStart() {
        super.onStart();

    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            result = GameView.result;
            if(result)
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog_win.show();
                    GameView.result = false;
                    GameSound.PlaySound(GameSound.complete, (float) 0.5);
                }
            });
        }
    }
}


/////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////https://code.tutsplus.com/ru/tutorials/getting-started-with-cloud-firestore-for-android--cms-30382/////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////