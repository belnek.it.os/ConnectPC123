package com.example.belnek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;


public class Levels extends AppCompatActivity implements View.OnClickListener {
public static TextView level1, level2, level3, level4, level5, level6, level7, level8, level9, level10;
public static Button exitAcc;
private Button button_back;
private Map<String, Object>progressMap = new HashMap();
public FirebaseFirestore progressDB;
private DocumentReference doc;
private FirebaseAuth mAuth;
public static long progress;
public static int level;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);
        level1 = (TextView) findViewById(R.id.level1);
        level2 = (TextView) findViewById(R.id.level2);
        level3 = (TextView) findViewById(R.id.level3);
        level4 = (TextView) findViewById(R.id.level4);
        level5 = (TextView) findViewById(R.id.level5);
        level6 = (TextView) findViewById(R.id.level6);
        level7 = (TextView) findViewById(R.id.level7);
        level8 = (TextView) findViewById(R.id.level8);
        level9 = (TextView) findViewById(R.id.level9);
        level10 = (TextView) findViewById(R.id.level10);
        exitAcc = (Button) findViewById(R.id.exitAcc);

        button_back = (Button) findViewById(R.id.button_back);
        level1.setOnClickListener(this);
        level2.setOnClickListener(this);
        button_back.setOnClickListener(this);
        exitAcc.setOnClickListener(this);
        progressDB = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();




    }
@Override
public void onStart() {
    super.onStart();
    level1.setVisibility(View.INVISIBLE);
    level2.setVisibility(View.INVISIBLE);
    level3.setVisibility(View.INVISIBLE);
    level4.setVisibility(View.INVISIBLE);
    level5.setVisibility(View.INVISIBLE);
    level6.setVisibility(View.INVISIBLE);
    level7.setVisibility(View.INVISIBLE);
    level8.setVisibility(View.INVISIBLE);
    level9.setVisibility(View.INVISIBLE);
    level10.setVisibility(View.INVISIBLE);

    doc = progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail());
    doc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();

                progressMap = document.getData();
                UpdateUILevels ((long) progressMap.get ("progress"));
            }
        }
    });


}


    @Override
    public void onClick(View v) {
        GameSound.PlaySound(GameSound.buttonClick, 1);
        switch (v.getId()){
            case R.id.level1:
                level = 1;
                startActivity(new Intent(Levels.this, Level1.class));
                break;
            case R.id.button_back:
                startActivity(new Intent(Levels.this, StartActivity.class));
                break;
            case R.id.level2:
                level = 2;
                startActivity(new Intent(Levels.this, Level1.class));
                break;
            case R.id.exitAcc:
                mAuth.signOut();
                Toast.makeText(getApplicationContext(), "Вы вышли из аккаунта.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Levels.this, LoginActivity.class));
        }

    }
    public static void UpdateUILevels (long progress){
        switch ((int) progress){
            case 1:
                level1.setVisibility (View.VISIBLE);
                break;
            case 2:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                break;
            case 3:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                break;
            case 4:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                break;
            case 5:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                break;
            case 6:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                level6.setVisibility (View.VISIBLE);
                break;
            case 7:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                level6.setVisibility (View.VISIBLE);
                level7.setVisibility (View.VISIBLE);

                break;
            case 8:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                level6.setVisibility (View.VISIBLE);
                level7.setVisibility (View.VISIBLE);
                level8.setVisibility (View.VISIBLE);

                break;
            case 9:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                level6.setVisibility (View.VISIBLE);
                level7.setVisibility (View.VISIBLE);
                level8.setVisibility (View.VISIBLE);
                level9.setVisibility (View.VISIBLE);
                break;
            case 10:
                level1.setVisibility (View.VISIBLE);
                level2.setVisibility (View.VISIBLE);
                level3.setVisibility (View.VISIBLE);
                level4.setVisibility (View.VISIBLE);
                level5.setVisibility (View.VISIBLE);
                level6.setVisibility (View.VISIBLE);
                level7.setVisibility (View.VISIBLE);
                level8.setVisibility (View.VISIBLE);
                level9.setVisibility (View.VISIBLE);
                level10.setVisibility (View.VISIBLE);
                break;
        }
    }
}