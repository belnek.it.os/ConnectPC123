package com.example.belnek;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Source;
import com.google.protobuf.Any;

import java.util.HashMap;
import java.util.Map;

import static com.google.android.gms.common.util.CollectionUtils.mapOf;

public class LoginActivity extends Activity implements View.OnClickListener {
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public FirebaseFirestore progressDB;
    private EditText ETemail;
    private EditText ETpassword;
    private Button back;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        progressDB = FirebaseFirestore.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                 user = firebaseAuth.getCurrentUser();


            }
        };

        ETemail = (EditText) findViewById(R.id.email);
        ETpassword = (EditText) findViewById(R.id.password);
        back = (Button) findViewById(R.id.back);

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.Sign).setOnClickListener(this);
    }
    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View view) {
        GameSound.PlaySound(GameSound.buttonClick, 1);
        if(view.getId() == R.id.login)
        {
            if (ETemail.getText().toString().equalsIgnoreCase("") || ETpassword.getText().toString().equalsIgnoreCase(""))

                Toast.makeText(LoginActivity.this, "Ни одно поле не должно оставатся пустым.", Toast.LENGTH_SHORT).show();
            else if (ETemail.getText().toString().contains(" ") == true || ETpassword.getText().toString().contains(" ") == true)
            Toast.makeText(LoginActivity.this, "Почта или пароль не могут содержать пробелы", Toast.LENGTH_SHORT).show();


            else
            signin(ETemail.getText().toString(),ETpassword.getText().toString());
        }
        else if (view.getId() == R.id.Sign)
        {
            if (ETemail.getText().toString().equalsIgnoreCase("") || ETpassword.getText().toString().equalsIgnoreCase(""))

                Toast.makeText(LoginActivity.this, "Не одно поле не должно оставатся пустым.", Toast.LENGTH_SHORT).show();

            else
            registration(ETemail.getText().toString(),ETpassword.getText().toString());
        }
        else if (view.getId() == R.id.back) {
            startActivity(new Intent(LoginActivity.this, StartActivity.class));
        }


    }

    public void signin(String email , String password)
    {
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Aвторизация успешна.", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(LoginActivity.this, StartActivity.class));
                    StartActivity.userSigned = true;



                }else
                    Toast.makeText(LoginActivity.this, "Aвторизация провалена. Может,  введены неверные данные?", Toast.LENGTH_LONG).show();

            }
        });
    }

    public void registration (String email , String password){
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {
                    Toast.makeText(LoginActivity.this, "Регистрация успешна. Можете авторизироваться.", Toast.LENGTH_LONG).show();
                    Map<String, Object> progressMap = new HashMap<>();
                    int progress = 1;
                    progressMap.put("progress", progress);
                    progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail()).set(progressMap);

                    // Успешная запись




                }
                else
                    Toast.makeText(LoginActivity.this, "Регистрация провалена. Попробуйте еще раз.", Toast.LENGTH_LONG).show();
            }
        });
    }
    }
    /////////////////////////////    https://www.chess.com/analysis/game/live/15774366961?tab=report
////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////