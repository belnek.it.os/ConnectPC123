package com.example.belnek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    public static boolean userSigned = false;
    private Dialog dialogAbout;
    private Dialog dialogHelp;
    private Button bst;
    private Button bbk;
    private Button abt;
    private Button sendButton;
    private Button backButtn;
    private Button helpb;
    private EditText help;
    public Map<String, Object> Help = new HashMap<>();
    private FirebaseFirestore progressDB;
    private FirebaseAuth mAuth;
    private DocumentReference doc;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Timer mTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        mAuth = FirebaseAuth.getInstance();
        progressDB = FirebaseFirestore.getInstance();
        dialogAbout = new Dialog(this);
        dialogAbout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAbout.setContentView(R.layout.dialog_aboutgame);
        dialogAbout.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAbout.setCancelable(false);
        dialogHelp = new Dialog(this);
        dialogHelp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogHelp.setContentView(R.layout.dialog_help);
        dialogHelp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogHelp.setCancelable(false);
        help = (EditText) dialogHelp.findViewById(R.id.editTextHelp);
        bst = (Button) findViewById(R.id.StartButton);
        bbk = (Button) dialogAbout.findViewById(R.id.back2);
        abt = (Button) findViewById(R.id.AboutGame);
        backButtn = (Button) dialogHelp.findViewById(R.id.back23);
        sendButton = (Button) dialogHelp.findViewById(R.id.Send);
        helpb = (Button) findViewById(R.id.Help);
        bst.setOnClickListener(this);
        bbk.setOnClickListener(this);
        abt.setOnClickListener(this);
        backButtn.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        helpb.setOnClickListener(this);
        GameSound.Init(getApplicationContext());
             }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null)
            userSigned = true;
        else
            userSigned = false;


    }

    @Override
    public void onClick(View v) {
        GameSound.PlaySound(GameSound.buttonClick, 1);
        switch (v.getId()) {
            case R.id.StartButton:
                if (userSigned) {
                    Intent intent = new Intent(StartActivity.this, Levels.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.back2:
                dialogAbout.dismiss();
                break;
            case R.id.AboutGame:
                dialogAbout.show();
                break;
            case R.id.back23:
                dialogHelp.dismiss();
                break;
            case R.id.Send:

                if (help.getText().toString().equalsIgnoreCase(""))

                    Toast.makeText(StartActivity.this, "Обращение к разработчикам не должно быть пустым.", Toast.LENGTH_SHORT).show();
                else if (help.getText().toString().length() < 31)
                    Toast.makeText(StartActivity.this, "Обращение к разработчикам должно содержать больше 30 символов.", Toast.LENGTH_SHORT).show();
                else {
                    Help.put("Help", help.getText().toString());
                    progressDB.collection("progress").document(mAuth.getCurrentUser().getEmail()).collection("Help").document("Help").set(Help);
                    Toast.makeText(getApplicationContext(), "Ваше обращение отправлено. Ждите ответ на почту.", Toast.LENGTH_SHORT);
                    dialogHelp.dismiss();


                }
                break;


            case R.id.Help:
                if (mAuth.getCurrentUser() != null)
                    dialogHelp.show();
                else {
                    Toast.makeText(getApplicationContext(), "Сначала надо авторизироваться!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(StartActivity.this, LoginActivity.class));

                }
        }


}
}


